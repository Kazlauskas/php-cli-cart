<?php

namespace App\Commands;

use LaravelZero\Framework\Commands\Command;
use Illuminate\Support\Facades\Artisan;

class MenuCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'menu';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Show main menu (start the application)';

    // The cart data is stored in memory, I tried to find some nicer console "session" solution or
    // state management, but didn't find the solution just yet. Decided to use an object in memory and class methods
    // to handle most of the operations.
    protected $cart;

    protected $defaultCurrency = 'EUR';



    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $optionProducts = 'See available products';
        $optionCart = 'See cart content';
        $optionAdd = 'Add item';
        $optionRemove = 'Remove item';
        $optionExit = 'Exit';
        
        $choice = $this->choice('What would you like to do?', [$optionProducts, $optionCart, $optionAdd, $optionRemove,  $optionExit], 0);
        
        // Surprisingly choice returns the label, not the index of an array, I don't know if it's a bug or intended behavior
        // So I have to switch the label here, didn't have time to dig deper, but that's not normal. Ideally should be enum/integer.

        switch($choice){
            case $optionProducts:
                // Show command could also go into this class, but I simply left it as a separate command to see how
                // different commands work together and how could they share the same code (that's why I have Toolbox.php)
                // Normally, one class should have all entity-related methods within to ensure single responsibility
                $this->call('show-products');
                $this->call('menu');
                break;
            case $optionAdd:
                $result = $this->addItem();
                if (!empty($result))
                {
                    $this->cart = \App\Toolbox::updateCart($this->cart, $result);
                }
                $this->showCart();
                $this->call('menu');
                break;
            case $optionRemove:
                $result = $this->removeItem();
                if (!empty($result))
                {
                    $this->cart = \App\Toolbox::updateCart($this->cart, $result);
                }
                $this->showCart();
                $this->call('menu');
                break;
            case $optionCart:
                $this->showCart();
                $this->call('menu');
                break;
            case $optionExit:
                break;
        }

        return;
    }

    // I don't substract/mark products from DB that are added to the cart. Ideally, you should mark that in the session,
    // so user would not add the same product again and again, if there is just 1 unit left. Right now it is possible
    // and is a known thing. I just check the total amount of the product.
    public function addItem()
    {
        $this->call('show-products');

        $items = \App\Toolbox::getItems(false);

        // This area should be improved, with long IDs in the future the selection should be done e.g. by having array keys resetted
        $chosenID = $this->ask('Please, type in ID of the product that you want to add to the cart');


        if (empty($items[$chosenID]))
        {
            $this->error('Product does not exist, please try again');
            return;
        }

        $chosenProduct = $items[$chosenID];
        $chosenAmount = $this->ask('You can add a maximum of ' .$chosenProduct['amount'] .' units of this product. Type in how many would you like to add.');

        if ($chosenAmount > $items[$chosenID]['amount'])
        {
            $this->error('You cannot add that many units');
            return;
        }

        // Confirmation - not entirely necessary, just wanted to play around and explore
        if ($this->confirm('Please confirm that you want to add ' .$chosenAmount . ' unit(s) of ' .$chosenProduct['name']))
        {
            $this->info($chosenAmount . ' units of ' .$chosenProduct['name'] .' were added to your cart');
        }

        return
        [
            'id' => $chosenID,
            'amount' => $chosenAmount
        ];
    }

    public function removeItem()
    {        
        if (!empty($this->cart))
        {
            $this->showCart();
            // This area should be improved, with long IDs in the future the selection should be done e.g. by having array keys resetted
            $chosenID = $this->ask('Please, type in ID of the product that you want to remove from the cart');


            if (empty($this->cart[$chosenID]))
            {
                $this->error('Product does not exist, please try again');
                return;
            }

            $chosenAmount = $this->ask('You can remove a maximum of ' .$this->cart[$chosenID] .' unit(s) of this product. Type in how many would you like to remove.');

            if ($chosenAmount > $this->cart[$chosenID])
            {
                $this->error('You cannot remove that many units');
                return;
            }

            $this->info('Product removed from the cart successfully');

            return
            [
                'id' => $chosenID,
                'amount' => $chosenAmount * -1 // To retain the same update functionality, but reduce the number
            ];
        }
    }

    public function showCart()
    {
        $items = \App\Toolbox::getItems();

        if (!empty($this->cart))
        {
            $cartItems = [];
            $total = 0;
            foreach ($this->cart as $id=>$amount)
            {
                if (!empty($items[$id]))
                {
                    $showThisItem = $items[$id];
                    $showThisItem['amount'] = $amount;
                    $result = \App\Toolbox::convertPrice($showThisItem['price'] * $amount, $showThisItem['currency'], $this->defaultCurrency);
                    if (!empty($result['error']))
                    {
                        $this->error($result['error']);
                    }
                    $total += $result['convertedAmount'];
                    $cartItems[] = $showThisItem;
                }
            }

            $this->info('Cart content');
            $this->table(['ID', 'Product name', 'Amount', 'Unit Price', 'Currency'], $cartItems);
            $this->info('Cart total: ' .$total .' ' .$this->defaultCurrency);
        }
        else
        {
            $this->info('The cart is empty');
        }
    }
}
