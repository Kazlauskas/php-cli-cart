<?php

namespace App;

// Class used to store shared functionality. Not very necessary for this project, but for larger ones I always have
// such repository of functions that are used across the project
class Toolbox
{
    public static function updateCart($cart, $data)
    {
        $id = $data['id'];
        if (!empty($cart[$id]))
        {
            $cart[$id] = $cart[$id] + $data['amount']; // Record already exists
        }
        else
        {
            $cart[$id] = $data['amount']; // New record
        }

        // Make sure to remove products with zero amount or less
        if (isset($cart[$id]) && $cart[$id] <= 0)
        {
            unset($cart[$id]);
        }

        return $cart;
    }

    public static function getItems($withEmpty = true)
    {
        $cleanData = [];
        foreach(file(storage_path('database.txt')) as $line)
        {
            $dataset = explode(';', $line); // This is a quick solution, for large datasets accesing an array using a key is not fast
            $id = $dataset[0];
            
            if(!$withEmpty && $dataset[2] <= 0) continue; // Can arguable be used with curly brackets, depends on the overall style

            $cleanData[$id] =
            [
                'id' => $dataset[0],
                'name' => $dataset[1],
                'amount' => $dataset[2],
                'price' => $dataset[3],
                'currency' => $dataset[4],
            ];
        }

        return $cleanData;
    }

    // Used to convert price from different currencies. This method serves a config for currency exchange ratio,
    // which normally should be stored in a proper config or external service.
    public static function convertPrice($price, $fromCurrency, $toCurrency)
    {
        $fromCurrency = trim($fromCurrency);
        $toCurrency = trim($toCurrency);

        if ($fromCurrency == $toCurrency)
        {
            $return['convertedAmount'] = $price;
            return $return;
        }

        // Currently we only support exchange to EUR from GBP and USD. We would need to add exchange
        // ratios if we want to convert vice versa.
        
        $ratios =
        [
            'EUR' =>
                [
                    'GBP' => 0.88,
                    'USD' => 1.14
                ],
            'NEW_CURRENCY' =>  // This is to demonstrate extension for new currencies only. This would contain
                [
                    'EUR' => 2.2,
                    'SGD' => 1.6
                ]
        ];

        if (!empty($ratios[$toCurrency]) && !empty($ratios[$toCurrency][$fromCurrency]))
        {
            $convertRatio = $ratios[$toCurrency][$fromCurrency];
        }
        else
        {
            // If we don't have the exchange ratio, we treat it as default currency ($toCurrency variable) and show a warning to the user
            $convertRatio = 1;
            $return['error'] = 'Warning! Your database contains currency that the app does not support. Cannot exchange from ' .$fromCurrency .' to ' .$toCurrency .'. Treating ' .$fromCurrency .' as ' .$toCurrency;
        }

        // This rounding is just for the sake of display.
        // With serious fintech apps this should probably handled many digits after comma
        // to keep acocunting and balances solid
        $return['convertedAmount'] = round($price / $convertRatio, 2);

        return $return;

    }

}
