<?php

namespace App\Commands;

use LaravelZero\Framework\Commands\Command;

class ShowProductsCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'show-products';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Show available products';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $items = \App\Toolbox::getItems(false);
        if (!empty($items))
        {
            $this->table(['ID', 'Product name', 'Amount', 'Price', 'Currency'], $items);
        }
        else
        {
            $this->info('There are no products');
        }        
    }
}
