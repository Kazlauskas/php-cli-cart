------

## Installation
- Go to project root
- Run "composer install"
- To start the application, run "php cart menu"

## Documentation

- For full command list, please run "php cart".
- Product data is stored at /storage/database.txt
- Current method comments do not have variables and their types described, to be added in the future

## Support
If you see strange characters in console output (e.g. [[33m0[39m]), there is a high chance that the problem is related to your console ANSI color support. If your console displays strange characters together with app output, try this solution: https://laravel.io/forum/09-28-2016-php-artisan-list-display-strange-characters
You can also download this console https://conemu.github.io/ that support ANSI colors.

The app is functional even with unsopported colors, if you want to go this way, look for the actual output that you will have to use to interact with the app. E.g. [[33m  ->0<- [39m] in this case "0" would be the real meaningful output.



